package io.taweesoft.firealarm.Activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gcm.GCMRegistrar;

import java.util.Observable;
import java.util.Observer;

import io.taweesoft.firealarm.AlertDialogFactory;
import io.taweesoft.firealarm.AlertDialog_Button;
import io.taweesoft.firealarm.Command;
import io.taweesoft.firealarm.Commands.LogoutCommand;
import io.taweesoft.firealarm.GCMIntentService;
import io.taweesoft.firealarm.Library.Database;
import io.taweesoft.firealarm.Library.LoadingDialog;
import io.taweesoft.firealarm.Library.PrepareData;
import io.taweesoft.firealarm.Library.Resources;
import io.taweesoft.firealarm.Library.Utility;
import io.taweesoft.firealarm.R;
import io.taweesoft.firealarm.RoomCustomAdapter;

public class FirstActivity extends ActionBarActivity implements Observer {

    private LoadingDialog loadingDialog;
    private ListView lv_room;
    private TextView txt_site,txt_location;
    public static RoomCustomAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);

        super.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        /*---- Register GCM on this device. ----*/
        GCMRegistrar.checkDevice(this);
        GCMRegistrar.checkManifest(this);
        GCMRegistrar.register(this, GCMIntentService.SENDER_ID);
        Utility.setActionBarColor(getSupportActionBar());
        loadingDialog = new LoadingDialog(this);
        lv_room = (ListView)findViewById(R.id.lv_room);
        txt_site = (TextView)findViewById(R.id.txt_site);
        txt_location = (TextView)findViewById(R.id.txt_location);
        prepareData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_first, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if(id == R.id.action_logout){
            AlertDialogFactory factory = AlertDialogFactory.getFactory();
            Command logoutCommand = new LogoutCommand(this);
            AlertDialog_Button yes = new AlertDialog_Button(logoutCommand);
            AlertDialog_Button no = new AlertDialog_Button(null);
            factory.createDialog(this,"Logout","Are you sure to logout ?" ,"YES",yes,"NO",no).show();
        }

        return super.onOptionsItemSelected(item);
    }

    public void prepareData(){
        loadingDialog.show();
        PrepareData prepareData = new PrepareData(this);
        prepareData.prepare();
    }
    @Override
    public void update(Observable observable, Object data) {
        if(data.getClass() == PrepareData.class) {
            loadingDialog.hide();
            adapter = new RoomCustomAdapter(this, Resources.rooms);
            lv_room.setAdapter(adapter);
            txt_site.setText(Resources.site.get("name"));
            txt_location.setText(Resources.site.get("location"));
            setListViewListener();
        }
    }

    public void setListViewListener(){
        lv_room.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(FirstActivity.this,RoomActivity.class);
                String room_no = Resources.rooms.get(position);
                intent.putExtra("room_no",room_no);
                startActivity(intent);
            }
        });
    }


}
