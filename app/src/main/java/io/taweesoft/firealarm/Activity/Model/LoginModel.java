package io.taweesoft.firealarm.Activity.Model;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;

import org.json.JSONException;

import java.util.Observable;
import java.util.Observer;

import io.taweesoft.firealarm.Activity.Holder.LoginHolder;
import io.taweesoft.firealarm.Activity.LoginActivity;
import io.taweesoft.firealarm.Library.Database;
import io.taweesoft.firealarm.Library.LoadingDialog;
import io.taweesoft.firealarm.Library.Resources;
import io.taweesoft.firealarm.User;

/**
 * Created by TAWEESOFT on 8/2/2015 AD.
 */
public class LoginModel extends Observable{
    private Observer observer;
    private LoginHolder holder;
    private SharedPreferences prefs;
    public LoginModel(Observer observer,LoginHolder holder){
        this.observer = observer;
        this.holder = holder;
        prefs = PreferenceManager.getDefaultSharedPreferences(holder.acitivity);
    }

    public void doLogin(){
        AsyncTask<Void,Void,User> task = new AsyncTask<Void, Void, User>() {
            @Override
            protected User doInBackground(Void... params) {
                try {
                    Resources.user = Database.checkForLogin(holder.txt_username.getText().toString(), holder.txt_password.getText().toString());
                    return Resources.user;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(User user) {
                observer.update(LoginModel.this,user);
            }
        }.execute();
    }

    public void saveLoginSession(){
        prefs.edit().putString("username" , Resources.user.getUsername()).commit();
        prefs.edit().putInt("permission", Resources.user.getPermission()).commit();
    }

    public void checkAutoLogin(){
        String username = prefs.getString("username",null);
        if(username == null){
            return;
        }
        int permission = prefs.getInt("permission",-1);
        Resources.user = new User(username,permission);
        observer.update(LoginModel.this, Resources.user);
    }


}
