package io.taweesoft.firealarm.Activity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.animation.AccelerateDecelerateInterpolator;

import java.util.Map;

import io.taweesoft.firealarm.Activity.Holder.RoomHolder;
import io.taweesoft.firealarm.Library.Database;
import io.taweesoft.firealarm.Library.Resources;
import io.taweesoft.firealarm.Library.Utility;
import io.taweesoft.firealarm.PieGraphFolder.PieGraph;
import io.taweesoft.firealarm.PieGraphFolder.PieSlice;
import io.taweesoft.firealarm.R;


public class RoomActivity extends ActionBarActivity {
    private RoomHolder holder;
    private String room_no;
    private String key;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room);
        super.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        Utility.setActionBarColor(getSupportActionBar());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        holder = new RoomHolder(this);
        room_no = getIntent().getStringExtra("room_no");
        if(room_no.equals("401"))
            key = "web1";
        else key = "web2";
        setTitle(room_no+"");
        PieSlice slice = new PieSlice();
        if(Resources.data.get(room_no) != null){
            Map<String,String> data = Resources.data.get(room_no);
            int status = Integer.parseInt(data.get("status"));
            if(status == 0){
                slice.setValue(0);
                slice.setColor(Color.parseColor("#F44336"));
                slice.setGoalValue(1);
                holder.pieGraph.addSlice(slice);
                slice = new PieSlice();
                slice.setColor(Color.parseColor("#EF9A9A"));
                slice.setValue(1);
                holder.pieGraph.addSlice(slice);
            }else if(status == 1){
                slice.setValue(0);
                slice.setColor(Color.parseColor("#FFC107"));
                slice.setGoalValue(1);
                holder.pieGraph.addSlice(slice);
                slice = new PieSlice();
                slice.setColor(Color.parseColor("#FFE082"));
                slice.setValue(1);
                holder.pieGraph.addSlice(slice);
            }else if(status == 2){
                slice.setValue(0);
                slice.setGoalValue(2);
                slice.setColor(Color.parseColor("#00BCD4"));
                holder.pieGraph.addSlice(slice);
                slice = new PieSlice();
                slice.setColor(Color.parseColor("#80DEEA"));
                slice.setValue(1);
                holder.pieGraph.addSlice(slice);
            }else if(status == 3){
                slice.setValue(0);
                slice.setGoalValue(2);
                slice.setColor(Color.parseColor("#8BC34A"));
                holder.pieGraph.addSlice(slice);
                slice = new PieSlice();
                slice.setColor(Color.parseColor("#C5E1A5"));
                slice.setValue(1);
                holder.pieGraph.addSlice(slice);
            }
//            for (PieSlice s : holder.pieGraph.getSlices())
//                s.setGoalValue(2);
            holder.pieGraph.setDuration(4000);//default if unspecified is 300 ms
            holder.pieGraph.setInterpolator(new AccelerateDecelerateInterpolator());//default if unspecified is linear; constant speed
//            holder.pieGraph.setAnimationListener(getAnimationListener());//optional
            holder.pieGraph.animateToGoalValues();
            holder.img_icon.setImageResource(Resources.statusImgArr[status]);
            holder.txt_status.setText(Resources.statusArr[status]);
            holder.txt_type.setText(Utility.getStatusText(status));
            holder.img_type.setImageResource(Resources.statusImgCircleArr[status]);
        }else{
            slice.setValue(1);
            slice.setColor(Color.parseColor("#8BC34A"));
            holder.pieGraph.addSlice(slice);
            holder.txt_type.setText("Normal");
            holder.txt_status.setText("Your room is secure");
            holder.img_icon.setImageResource(Resources.statusImgArr[3]);
            holder.img_type.setImageResource(R.drawable.safe);
        }

        holder.txt_room.setText(room_no);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if(Resources.user.getPermission() == 0)
            getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if(id == android.R.id.home){
            this.finish();
        }else if(id == R.id.action_fireman_arrived){
            AsyncTask<Void,Void,Void> updateStatus = new UpdateStatus(1);
            updateStatus.execute();
        }else if(id == R.id.action_extinguished){
            AsyncTask<Void,Void,Void> updateStatus = new UpdateStatus(2);
            updateStatus.execute();
        }else if(id == R.id.action_normal){
            AsyncTask<Void,Void,Void> updateStatus = new UpdateStatus(3);
            updateStatus.execute();
        }

        return super.onOptionsItemSelected(item);
    }

    class UpdateStatus extends AsyncTask<Void,Void,Void>{
        private int status;
        public UpdateStatus(int status){
            this.status = status;
        }
        @Override
        protected Void doInBackground(Void... params) {
            Database.changeStatus(room_no,key,status);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Intent intent = RoomActivity.this.getIntent();
            overridePendingTransition(0, 0);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            RoomActivity.this.finish();
            overridePendingTransition(0, 0);
            startActivity(intent);
//                    getWindow().getDecorView().findViewById(android.R.id.content).invalidate();
        }
    }


}
