package io.taweesoft.firealarm.Activity.Holder;

import android.app.Activity;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import io.taweesoft.firealarm.PieGraphFolder.PieGraph;
import io.taweesoft.firealarm.R;

/**
 * Created by TAWEESOFT on 8/2/2015 AD.
 */
public class RoomHolder {
    public TextView txt_room,txt_type,txt_status;
    public PieGraph pieGraph;
    public ImageView img_icon,img_type;
    public Activity activity;

    public RoomHolder(Activity activity){
        this.activity = activity;
        pieGraph = (PieGraph)activity.findViewById(R.id.pieGraph);
        pieGraph.setInnerCircleRatio(230);
        txt_room = (TextView)activity.findViewById(R.id.txt_room);
        txt_type = (TextView)activity.findViewById(R.id.txt_type);
        txt_status = (TextView)activity.findViewById(R.id.txt_status);
        img_icon = (ImageView)activity.findViewById(R.id.img_icon);
        img_type = (ImageView)activity.findViewById(R.id.img_type);
    }
}
