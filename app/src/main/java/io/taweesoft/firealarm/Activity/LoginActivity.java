package io.taweesoft.firealarm.Activity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import org.json.JSONException;

import java.util.Observable;
import java.util.Observer;

import io.taweesoft.firealarm.Activity.Holder.LoginHolder;
import io.taweesoft.firealarm.Activity.Model.LoginModel;
import io.taweesoft.firealarm.Library.Database;
import io.taweesoft.firealarm.Library.LoadingDialog;
import io.taweesoft.firealarm.Library.Utility;
import io.taweesoft.firealarm.R;
import io.taweesoft.firealarm.User;

public class LoginActivity extends ActionBarActivity implements Observer {
    private LoginHolder holder;
    private LoadingDialog loadingDialog;
    private LoginModel model;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utility.setActionBarColor(getSupportActionBar());
        super.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_login);
        holder = new LoginHolder(this);
        loadingDialog = new LoadingDialog(this);
        model = new LoginModel(this,holder);
        setLoginButtonListener();
        model.checkAutoLogin();
    }

    public void setLoginButtonListener(){
        holder.btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadingDialog.show();
                model.doLogin();
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        return super.onOptionsItemSelected(item);
    }

    @Override
    public void update(Observable observable, Object data) {
        if(observable.getClass() == LoginModel.class){
            loadingDialog.hide();
            if(data != null){
                model.saveLoginSession();
                Intent intent = new Intent(this,FirstActivity.class);
                startActivity(intent);
                Toast.makeText(this,"Welcome", Toast.LENGTH_SHORT).show();
                this.finish();
            }else{
                Toast.makeText(this,"Wrong username or password", Toast.LENGTH_SHORT).show();
            }

        }
    }
}
