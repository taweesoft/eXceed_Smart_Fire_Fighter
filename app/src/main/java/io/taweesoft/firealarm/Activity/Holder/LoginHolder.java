package io.taweesoft.firealarm.Activity.Holder;

import android.app.Activity;
import android.widget.Button;
import android.widget.TextView;

import io.taweesoft.firealarm.R;

/**
 * Created by TAWEESOFT on 8/2/2015 AD.
 */
public class LoginHolder {
    public TextView txt_username,txt_password;
    public Button btn_login;
    public Activity acitivity;
    public LoginHolder(Activity activity){
        this.acitivity = activity;
        this.txt_username = (TextView)activity.findViewById(R.id.txt_username);
        this.txt_password = (TextView)activity.findViewById(R.id.txt_password);
        btn_login = (Button)activity.findViewById(R.id.btn_login);
    }
}
