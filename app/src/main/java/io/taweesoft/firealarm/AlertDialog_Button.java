package io.taweesoft.firealarm;

import android.content.DialogInterface;


/**
 * Created by TAWEESOFT on 7/27/2015 AD.
 */
public class AlertDialog_Button implements DialogInterface.OnClickListener {

    public Command command;

    public AlertDialog_Button(Command command){
        this.command = command;
    }
    @Override
    public void onClick(DialogInterface dialog, int which) {
        if(command != null)
            command.run();
    }
}
