package io.taweesoft.firealarm;

/**
 * Created by TAWEESOFT on 8/2/2015 AD.
 */
public class User {
    private String username;
    private int permission;

    public User(String username, int permission){
        this.username = username;
        this.permission = permission;
    }

    public String getUsername() {
        return username;
    }

    public int getPermission() {
        return permission;
    }
}
