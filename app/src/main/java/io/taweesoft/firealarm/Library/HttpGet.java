package io.taweesoft.firealarm.Library;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.InputStream;
import java.util.List;

/**
 * Created by TAWEESOFT on 8/1/2015 AD.
 */
public class HttpGet {
    public static String getData(String URL,String key) {
        try {
            HttpClient client = new DefaultHttpClient();
            String getURL = URL+"/" + key;
            org.apache.http.client.methods.HttpGet get = new org.apache.http.client.methods.HttpGet(getURL);
            HttpResponse responseGet = client.execute(get);
            HttpEntity resEntityGet = responseGet.getEntity();
            if (resEntityGet != null) {
                return EntityUtils.toString(resEntityGet);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void sendData(String key, String value){
        try {
            List<NameValuePair> pairs = JSONSQLReader.getNamePair("data",value);
            InputStream inputStream = JSONSQLReader.openConnection(pairs,Resources.URL+"/"+key);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
