package io.taweesoft.firealarm.Library;

import android.app.Activity;
import android.app.Dialog;
import android.view.Window;
import android.widget.TextView;

import android.app.Activity;
import android.app.Dialog;
import android.view.Window;
import android.widget.TextView;

import io.taweesoft.firealarm.R;

/**
 * Created by TAWEESOFT on 7/27/2015 AD.
 */
public class LoadingDialog {

    private Activity activity;
    private Dialog dialog;
    private TextView txt_msg;

    public LoadingDialog(Activity activity){
        this.activity = activity;
        dialog = new Dialog(activity);
        dialog.setCancelable(false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_loading_progress);
        txt_msg = (TextView)dialog.findViewById(R.id.txt_msg);
    }

    public void show(){
        dialog.show();
    }

    public void hide(){
        dialog.dismiss();
    }

    public void setMessage(String message){
        txt_msg.setText(message);
    }
}
