package io.taweesoft.firealarm.Library;

import android.util.Log;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.taweesoft.firealarm.User;

/**
 * Created by TAWEESOFT on 8/1/2015 AD.
 */
public class Database {
    public static void getSite(){
        String data = HttpGet.getData(Resources.URL,"site_name");
        String[] dataArr = data.split(",");
        Resources.site.put("name",dataArr[0]);
        Resources.site.put("location",dataArr[1]);
    }

    public static void getRooms(){
        if(Resources.rooms == null)
            Resources.rooms = new ArrayList<String>();
        else
            Resources.rooms.clear();
        String data = HttpGet.getData(Resources.URL,"rooms");
        String[] dataArr = data.split(",");
        for(String each : dataArr){
            Log.d("xxx : " , each);
            Resources.rooms.add(each);
        }
    }

    public static void getInformation() throws JSONException {
        if(Resources.data == null){
            Resources.data = new HashMap<String,Map<String,String>>();
        }else Resources.data.clear();
        String data ="";
//        String data = HttpGet.getData(Resources.URL,"board1");
        String boardInfo = HttpGet.getData(Resources.URL,"boardInfo");
        List<Map<String,String>> mapBoardInfo = convertStringToMap(boardInfo,new String[]{"board","location"});
        for(Map<String,String> each : mapBoardInfo){
            Resources.board.put(each.get("location"),each.get("board"));
            data += each.get("location")+",";
            data += HttpGet.getData(Resources.URL,each.get("board"));
            data += "/";
        }
        Log.d("XXXXX : " , data);
        String[] dataArr = data.split("/");
        for(String each : dataArr){
            Map<String,String> map = new HashMap<String,String>();
            String[] eachArr = each.split(",");
            for(int i=0;i<eachArr.length;i++){ map.put(Resources.dataColumn[i],eachArr[i]); }
            Resources.data.put(map.get("location"), map);
        }
    }

    public static void saveDeviceKey(){
        String old_noti = HttpGet.getData(Resources.URL,"noti");
        if(!old_noti.contains(Resources.deviceID)){
            old_noti += "," + Resources.deviceID;
            HttpGet.sendData("noti",old_noti);
        }
    }

    public static void changeStatus(String room_no,String key, int status){
//        String key = Resources.board.get(room_no);
        HttpGet.sendData(key, status+"");
        Resources.data.get(room_no).put("status",status+"");
//        if(Resources.data.get(room_no) != null) {
//            Resources.data.get(room_no).put("status", status + "");
//            String data = "";
//            for (Map.Entry<String, Map<String, String>> each : Resources.data.entrySet()) {
//                Map<String, String> eachRoom = Resources.data.get(each.getKey());
//                for (int i = 0; i < Resources.dataColumn.length; i++) {
//                    data += eachRoom.get(Resources.dataColumn[i]);
//                    if (Resources.dataColumn.length - i != 1) data += ",";
//                }
//                data += "/";
//            }
//            HttpGet.sendData("data", data);
//        }
    }

    public static User checkForLogin(String username, String password) throws JSONException {
        User user = null;
        String data = HttpGet.getData(Resources.URL,"users");
        List<Map<String,String>> dataList = convertStringToMap(data,new String[]{"username","password","permission"});
        for(Map<String,String> each : dataList){
            String server_user = each.get("username");
            Log.d("XXX : " , server_user);
            String server_pass = each.get("password");
            Log.d("XXX : " , server_pass);
            int permission = Integer.parseInt(each.get("permission"));
            if(server_user.equals(username) && server_pass.equals(password)){
                user = new User(server_user,permission);
                break;
            }
        }
        return user;
    }

    public static List<Map<String,String>> convertStringToMap(String data,String[] columnName) throws JSONException {
        List<Map<String,String>> dataList = new ArrayList<Map<String,String>>();

        JSONArray jsonArray = new JSONArray(data);
        for(int i=0;i<jsonArray.length();i++){
            JSONObject obj = jsonArray.getJSONObject(i);
            Map<String,String> map = new HashMap<String,String>();
            for(int k=0;k<columnName.length;k++){
                Log.d("eeee : " , obj.getString(columnName[k]));
                map.put(columnName[k],obj.getString(columnName[k]));
            }
            dataList.add(map);
        }
        return dataList;
    }
}
