package io.taweesoft.firealarm.Library;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.ActionBar;

/**
 * Created by TAWEESOFT on 8/2/2015 AD.
 */
public class Utility {
    public static void setActionBarColor(ActionBar actionBar) {
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00BCD4")));
    }

    public static String getStatusText(int status){
        switch (status){
            case 0:
                return "On fire";
            case 1:
                return "Extinguishing";
            case 2:
                return "Extinguished";
            case 3:
                return "Normal";
        }
        return null;
    }
}
