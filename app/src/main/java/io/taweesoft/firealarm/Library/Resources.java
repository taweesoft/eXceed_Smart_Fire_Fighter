package io.taweesoft.firealarm.Library;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.taweesoft.firealarm.R;
import io.taweesoft.firealarm.User;

/**
 * Created by TAWEESOFT on 8/1/2015 AD.
 */
public class Resources {
    public static User user;
    public static Map<String,String> site = new HashMap<String,String>();
    public static Map<String,String> board = new HashMap<String,String>();
    public static String deviceID;
    public static List<String> rooms;
    public static Map<String,Map<String,String>> data = null;
    public static final String URL = "http://exceed.cupco.de/iot/linkceed";
    public static final String[] dataColumn = new String[]{"location","status"};
    public static final String[] statusArr = new String[]{"Waiting for fireman","Fireman arrived","Waiting for verified","Your room is secure"};
    public static final int[] statusImgArr = new int[]{
            R.drawable.fire,
            R.drawable.water,
            R.drawable.loading,
            R.drawable.safe
    };
    public static final int[] statusImgCircleArr = new int[]{
            R.drawable.fire_circle,
            R.drawable.water_circle,
            R.drawable.extinguished_circle,
            R.drawable.normal_circle
    };
    public static final int[] sprinkler = new int[]{
            R.drawable.sprinkler,
            0,
            0,
            0
    };
}
