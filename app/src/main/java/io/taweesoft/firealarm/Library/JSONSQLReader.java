package io.taweesoft.firealarm.Library;

import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by TAWEESOFT on 7/1/15.
 * How to use
 *  List<NameValuePair> pairs = getNamePair("SELECT id,name,mode FROM tests order by id");
    InputStream inputStream = openConnection(pairs);
    List<Map<String,String>> data = getData(inputStream,new String[] {"id","name","mode"} );
 */
public class JSONSQLReader {

    public String getData(String URL) {
        try {
            HttpClient client = new DefaultHttpClient();
            String getURL = URL;
            HttpGet get = new HttpGet(getURL);
            HttpResponse responseGet = client.execute(get);
            HttpEntity resEntityGet = responseGet.getEntity();
            if (resEntityGet != null) {
               return EntityUtils.toString(resEntityGet);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    public static List<NameValuePair> getNamePair (String parameter ,String sql) {
        List<NameValuePair> pairs = new ArrayList<NameValuePair>();
        pairs.add(new BasicNameValuePair(parameter,sql));
        return pairs;
    }

    public static InputStream openConnection (List<NameValuePair> pairs,String url) {
        InputStream inputStream = null;
        try{
            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url);
            httpPost.setEntity(new UrlEncodedFormEntity(pairs));
            HttpResponse response = httpClient.execute(httpPost);
            HttpEntity entity = response.getEntity();
            inputStream = entity.getContent();
        }catch(Exception e) {
            Log.e("db_connection_err", e.toString());
        }
        return inputStream;
    }

    public static List<Map<String,String>> getData (InputStream inputStream, String[] tag) {
        StringBuilder sb = new StringBuilder();
        try{
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            String line = "";
            while((line = reader.readLine()) != null) sb.append(line + "\n");
        }catch(Exception e ) { Log.e("getData_err" , e.toString()); }
        Log.d("SB : ", sb.toString());
        List<Map<String,String>> data = new ArrayList<Map<String,String>>();
        try{
            JSONArray array = new JSONArray(sb.toString());
            for(int i=0; i<array.length();i++){
                Map<String,String> dataPerRow = new HashMap<String,String>();
                JSONObject object = array.getJSONObject(i);
                for(int k=0;k<tag.length;k++)
                    dataPerRow.put(tag[k],(String)object.get(tag[k]));
                data.add(dataPerRow);
            }
        }catch(Exception e) { Log.e("read_data_err" , e.toString()); }

        return data;
    }
}
