package io.taweesoft.firealarm.Library;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;

/**
 * Created by TAWEESOFT on 7/2/15.
 */
public class AlertDialogFactory {
    private static AlertDialogFactory factory = null;

    public static AlertDialogFactory getFactory() {
        if(factory == null) factory = new AlertDialogFactory();
        return factory;
    }

    public Dialog createDialog(Context context,String title,String message,String positiveTitle, DialogInterface.OnClickListener positiveButton,String negativeTitle,DialogInterface.OnClickListener negativeButton) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        if(title != null) dialog.setTitle(title);
        if(positiveButton != null) dialog.setPositiveButton(positiveTitle, positiveButton);
        if(negativeButton != null) dialog.setNegativeButton(negativeTitle,negativeButton);
        dialog.setMessage(message);
        return dialog.create();
    }

    class No implements DialogInterface.OnClickListener{
        @Override
        public void onClick(DialogInterface dialog, int which) {

        }
    }

    public DialogInterface.OnClickListener getEmptyNO() {
        return new No();
    }
}
