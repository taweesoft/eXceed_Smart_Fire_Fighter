package io.taweesoft.firealarm.Library;

import android.os.AsyncTask;

import org.json.JSONException;

import java.util.Observer;

import io.taweesoft.firealarm.Activity.FirstActivity;
import io.taweesoft.firealarm.Activity.RoomActivity;

/**
 * Created by TAWEESOFT on 8/2/2015 AD.
 */
public class PrepareData {
    private Observer observer;

    public PrepareData(Observer observer){
        this.observer = observer;
    }

    public void prepare(){
        AsyncTask<Void,Void,Void> task = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                Database.getSite();
                try {
                    Database.getInformation();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Database.getRooms();
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                if(observer != null)
                    observer.update(null,PrepareData.this);
                if(FirstActivity.adapter != null) FirstActivity.adapter.notifyDataSetChanged();
            }
        }.execute();
    }
}
