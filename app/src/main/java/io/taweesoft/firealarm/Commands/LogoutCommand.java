package io.taweesoft.firealarm.Commands;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import io.taweesoft.firealarm.Activity.LoginActivity;
import io.taweesoft.firealarm.Activity.Model.LoginModel;
import io.taweesoft.firealarm.Command;

/**
 * Created by TAWEESOFT on 8/2/2015 AD.
 */
public class LogoutCommand implements Command {
    private Activity activity;
    public LogoutCommand(Activity activity) {
        this.activity = activity;
    }
    @Override
    public void run() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(activity);
        prefs.edit().clear().commit();
        Intent intent = new Intent(activity,LoginActivity.class);
        activity.startActivity(intent);
        activity.finish();
    }
}
