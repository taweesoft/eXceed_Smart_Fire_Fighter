package io.taweesoft.firealarm;

/**
 * Created by TAWEESOFT on 7/9/15.
 */
public interface Command {
    void run();
}
