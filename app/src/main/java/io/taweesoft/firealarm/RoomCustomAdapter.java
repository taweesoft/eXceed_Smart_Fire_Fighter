package io.taweesoft.firealarm;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.taweesoft.firealarm.Library.Resources;


public class RoomCustomAdapter extends ArrayAdapter<String> {
    public RoomCustomAdapter(Context context,List<String> rooms){
        super(context,0,rooms);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;
        String room_name = getItem(position);
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.activity_custom_room,null);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        }else{
            holder = (Holder)convertView.getTag();
        }

        int state=3;
        if(Resources.data.get(room_name) != null){
            state = Integer.parseInt(Resources.data.get(room_name).get("status"));
        }
        Log.d("XXXX : " , state+"");
        holder.txt_room.setText(room_name);
        String status = Resources.statusArr[state];
        holder.img_status.setImageResource(Resources.statusImgCircleArr[state]);
        holder.img_sprinkler.setImageResource(Resources.sprinkler[state]);
        holder.txt_status.setText(status);
        return convertView;
    }

    class Holder {
        public TextView txt_room,txt_status;
        public ImageView img_status,img_sprinkler;
        public Holder(View view){
            txt_room = (TextView)view.findViewById(R.id.txt_room);
            img_status = (ImageView)view.findViewById(R.id.img_status);
            txt_status = (TextView)view.findViewById(R.id.txt_status);
            img_sprinkler = (ImageView)view.findViewById(R.id.img_sprinkler);
        }


    }
}
